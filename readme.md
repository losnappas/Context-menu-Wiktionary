# Context Menu Dictionary

Wiktionary lives in the context menu.

A Firefox web extension. Requires FF57. [https://addons.mozilla.org/en-US/firefox/addon/right-click-wiktionary/](https://addons.mozilla.org/en-US/firefox/addon/right-click-wiktionary/)

To try it, 

* Clone this repository
* Init submodules with `git submodule update --init`
* Head to `about:debugging`
* Load the manifest.json as temporary addon.

Unlicense'd. Icons are from Wiktionary/Wikipedia and licensed under whatever license they come with.

